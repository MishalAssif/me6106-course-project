\par We finally get to the crux of this article, the Newmark method. In this section we will introduce the Newmark family of time integration algorithms for mechanical systems with Lagrangian $L$ of the form 
\begin{equation}\label{eq: mass-spring}
L(q, \dot{q}) = \frac{1}{2}\dot{q}^TM\dot{q} - \frac{1}{2}q^TKq, \ M = M^T \succ 0, K = K^T \succeq 0.
\end{equation}
We will then go on to prove that the Newmark family of integrators are variational in nature. We will then discuss some implications of this. In the next chapter, we will verify through simulations the properties we prove in this section.

\section{Newmark Schemes}

We have already shown that the EL equations corresponding to the Lagrangian \ref{eq: mass-spring} is given by
\begin{equation}\label{eq: mass-spring-el}
M\ddot{q} + Kq = 0
\end{equation}

The Newmark method with constants 0 $\leq \gamma \leq 1, 0 \leq \beta \leq \frac{1}{2}$ is given by
\begin{align}\label{newmark}
q_{k+1} &= q_k + h\dot{q}_k + \frac{h^2}{2}((1-2\beta)a_k + 2\beta a_{k+1}) \\
\dot{q}_{k+1} &= \dot{q}_k + h((1-\gamma)a_k + \gamma a_{k+1}) \\
\end{align}
where 
\begin{equation}
a_k = -M^{-1}Kq_k	
\end{equation}
In equation \ref{newmark}, we have written the algorithm as an update of position of velocity. We shall rewrite the algorithm as an update of position alone. 
\begin{align}
q_{k+1} =& \ q_k + h\dot{q}_k + \frac{h^2}{2}((1-2\beta)a_k + 2\beta a_{k+1}) \\
q_{k+2} =& \ q_{k+1} + h\dot{q}_{k+1} + \frac{h^2}{2}((1-2\beta)a_{k+1} + 2\beta a_{k+2}) \\
\implies q_{k+2} - q_{k+1} =& \ q_{k+1} - q_k + h(\dot{q}_{k+1} - \dot{q}_k) \\ \nonumber
& +\frac{h^2}{2}((1-2\beta)(a_{k+1}-a_k) + 2\beta(a_{k+2}-a_{k+1})) \\
=& \ q_{k+1} - q_k + h^2((1-\gamma)a_k + \gamma a_{k+1}) \\ \nonumber
& +\frac{h^2}{2}((1-2\beta)(a_{k+1}-a_k) + 2\beta(a_{k+2}-a_{k+1})) \\
\end{align}
Therefore, the Newmark algorithm in the configuration update form is
\begin{align}\label{newmark-pos}
\frac{1}{h^2}(q_{k+2}-2q_{k+1}+q_k) = (\frac{1}{2}-\gamma+\beta)a_k + (\frac{1}{2}+\gamma-2\beta)a_{k+1} + \beta a_{k+2}
\end{align}

It is well known that for the Newmark method is second-order accurate if and only if $\gamma = \frac{1}{2}$, otherwise it is only consistent. Thus, $\gamma$ is usually taken equal to $\frac{1}{2}$. We will prove the equivalence of Newmark methods with $\gamma = \frac{1}{2}$ and variational methods in two stages :
\begin{enumerate}\label{eq-newmark}
	\item We first show that for $\gamma = \frac{1}{2}$ and any $\beta \leq \frac{1}{4}$, the Newmark method is equivalent to the variational method with discrete Lagrangian $L_d^{sym, \alpha}$ where $\alpha$ is chosen so that $\beta = \alpha(1-\alpha)$. This is relatively easy to show, as it follows from simple comparison between equations \ref{newmark-pos} and \ref{sym-variational}. 
	\item We will then establish that for $\gamma = \frac{1}{2}$ and any $0 \leq \beta \leq \frac{1}{2}$, there exists a discrete Lagrangian $L_d^{nm, \beta}$ such that the variational integrator corresponding to $L_d^{nm, \beta}$ is the same as the Newmark method. 
\end{enumerate}

\subsection{Newmark with $\gamma = \frac{1}{2}$ and $0 \leq \beta \leq \frac{1}{4}$}

\begin{theorem}\label{equivalence-1}
Newmark method with $\gamma = \frac{1}{2}$ and $\beta \leq \frac{1}{4}$ is equivalent to the $L_d^{sym, \alpha}$ variational algorithm with $\alpha$ chosen such that $\beta = \alpha(1-\alpha)$.
\end{theorem}
\begin{proof}
We recall the $L_d^{sym, \alpha}$ update \ref{sym-variational} 
\begin{align}\label{eq1}
\frac{1}{h^2}(q_{k+2}-2q_{k+1}+q_k) &= \frac{1}{2}((1-\alpha)a_{k+1+\alpha} + \alpha a_{k+2-\alpha} + \alpha a_{k+\alpha} + (1-\alpha)a_{k+1-\alpha}) \nonumber \\
&= \alpha(1-\alpha)a_k + (\alpha^2 + (1-\alpha)^2)a_{k+1} + \alpha(1-\alpha)a_{k+2}
\end{align}
When $\gamma = \frac{1}{2}$ the Newmark update can be written as
\begin{align}\label{eq2}
\frac{1}{h^2}(q_{k+2}-2q_{k+1}+q_k) = \beta a_k + (1-2\beta)a_{k+1} + \beta a_{k+2}
\end{align}
Comparing \ref{eq1} and \ref{eq2}, we see that if $\beta = \alpha(1-\alpha)$, the Newmark method and $L_d^{sym, \alpha}$ method are the same. Since $\beta \leq \frac{1}{4}$, we can find an $\alpha \in [0,1]$ such that $\beta = \alpha(1-\alpha)$.
\end{proof}

We recall some popular methods which belong to this class.
\begin{enumerate}
	\item $\gamma = \frac{1}{2}, \beta = 0$ : The Newmark algorithm in velocity update form can be written as
	\begin{align}
		q_{k+1} &= q_k + h\dot{q}_k + \frac{h^2}{2}a_k \\
		\dot{q}_{k+1} &= \dot{q}_k + \frac{h}{2}(a_k + a_{k+1}) \\
	\end{align}
	This is the well known \textit{Central difference scheme}. \\ 
	When $\beta = 0, \alpha(1-\alpha) = \beta \implies \alpha = 0$. So, the central difference scheme is equivalent to the the variational $L_d^{sym, \alpha}(\alpha = 0)$ algorithm.

	\item $\gamma = \frac{1}{2}, \beta = \frac{1}{4}$ : The Newmark algorithm in velocity update form can be written as
	\begin{align}
		q_{k+1} &= q_k + h\dot{q}_k + \frac{h^2}{4}(a_k + a_{k+1}) \\
		\dot{q}_{k+1} &= \dot{q}_k + \frac{h}{2}(a_k + a_{k+1}) \\
	\end{align}
	This is the well known \textit{Average accelaration method}. \\
	 When $\beta = \frac{1}{4}, \alpha(1-\alpha) = \beta \implies \alpha = \frac{1}{2}$. So, the average accelaration method is equivalent to the the variational $L_d^{sym, \alpha}(\alpha = \frac{1}{2})$ algorithm.
\end{enumerate}

\subsection{Newmark with $\gamma = \frac{1}{2}$ and $0 \leq \beta \leq \frac{1}{2}$}

In this section, we will prove that the Newmark method is variational in the more general case, where $0 \leq \beta \leq \frac{1}{2}$. To this end, consider the coordinate transformation
\begin{align}\label{coordinate}
x_k &= q_k + \beta h^2 M^{-1}Kq_k \\
&= (I + \beta h^2 M^{-1}K)q_k \\
&= A^{h, \beta}q_k.
\end{align}
where $A^{h, \beta} = I + \beta h^2 M^{-1}K$. For small enough $h, A^{h, \beta}$ is an invertible transformation.

\begin{theorem}\label{symp-newmark-gen}
Consider the discrete Lagrangian $L_d^{nm, \beta}$ given by
\begin{align}\label{disc-lag-nm}
L_d^{nm, \beta}(q_0, q_1) = \frac{h}{2}\bigl(\frac{q_1 - q_0}{h} \bigr)^T\bigl(A^{h, \beta}\bigr)^TM\bigl(A^{h, \beta}\bigr)\bigl(\frac{q_1 - q_0}{h}\bigr) - hq_0^T\bigl(A^{h, \beta}\bigr)^TKq_0.
\end{align}
Then the $L_d^{nm, \beta}$ variational algorithm is equivalent to the Newmark algorithm with $\gamma = \frac{1}{2}$ and $0 \leq \beta \leq \frac{1}{2}$.
\end{theorem}

\begin{proof}
We first rewrite the Newmark update scheme given in Equation \ref{newmark-pos} in terms of $x_k$. When $\gamma = \frac{1}{2}$, \ref{newmark-pos} can be written as
\begin{align*}
&\frac{1}{h^2}(q_{k+2} - 2q_{k+1}+q_k) = \beta a_k + (1-2\beta)a_{k+1} + \beta a_{k+2} \\
\implies & (q_{k+2} - \beta h^2a_{k+2}) - 2(q_{k+1}-\beta h^2a_{k+1}) + (q_{k} - \beta h^2a_{k}) = h^2 a_{k+1} \\
\implies & (I + \beta h^2 M^{-1}K)q_{k+2} - 2(I + \beta h^2 M^{-1}K)q_{k+1} \\ 
& + (I + \beta h^2 M^{-1}K)q_{k} = -h^2M^{-1}Kq_{k+1} \nonumber \\
\implies & A^{h, \beta}q_{k+2} - 2A^{h, \beta}q_{k+1} + A^{h, \beta}q_k + h^2M^{-1}Kq_{k+1} = 0. 
\end{align*}
Now, if we look at the DEL equations corresponding to $L_d^{nm, \beta}$, we get 
\begin{align*}
&\bigl(A^{h, \beta}\bigr)^TM\bigl(A^{h, \beta}\bigr)(2q_{k+1}-q_k-q_{k+2}) - h^2\bigl(A^{h, \beta}\bigr)^TKq_{k+1} = 0 \\
\implies &M\bigl(A^{h, \beta}\bigr)(2q_{k+1}-q_k-q_{k+2}) - h^2Kq_{k+1} = 0 \\
\implies & \bigl(A^{h, \beta}\bigr)(2q_{k+1}-q_k-q_{k+2}) - h^2M^{-1}Kq_{k+1} \\
\implies & \bigl(A^{h, \beta}\bigr)(q_{k+2}-2q_{k+1}+q_{k}) + h^2M^{-1}Kq_{k+1} = 0 
\end{align*}
Therefore, we see that the DEL equations and the Newmark update equations are the same.
\end{proof}
\par In conclusion, we have shoen that all members of the Newmark family with $\gamma = \frac{1}{2}$ are equivalent to a Variational integrator. This means that the Newmark algorithms are symplectic, and preserve the corresponding symplectic form on the phase space and the discrete Lagrangian energy. We shall see this in action in the next chapter, where we present the simulation results on a 100,000 DOF mechanical system.
