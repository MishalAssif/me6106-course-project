function csd_project()

beta = 0.25;
gamma = 0.5;
h = 1;
n = 10000;

N = 1000;
M = speye(N);
K = speye(N);
C = speye(N);
F = speye(N, 1);
x_0 = speye(N, 1);
x_dot_0 = speye(N, 1)

K(1, 1) = 2;
K(1, 2) = -1;
K(N, N) = 2;
K(N, N-1) = -1;
for i = 2:N-1
  K(i, i-1) = -1;
  K(i, i+1) = -1;
  K(i, i) = 2;
endfor

for i=1:N
  x_0(i) = 1;
  x_dot_0(i) = 1;
  C(i, i) = 0;
  F(i) = 0;
endfor

[e, ns] = newmark_energy(M, K, N, x_0, x_dot_0, beta, gamma, h, n);
plot(1:n, e);
hold on;
plot(1:n, ns);
endfunction

function [e, norm_state] = newmark_energy(M, K, N, x_0, v_0, beta, gamma, h, n)
e = speye(n, 1);
norm_state = speye(n, 1);

x_next = speye(N, 1);
v_next = speye(N, 1);
a_next = speye(N, 1);
x_pred_next = speye(N, 1);
v_pred_next = speye(N, 1);
x_curr = x_0;
v_curr = v_0;
a_curr = speye(N, 1);

A = -1*inv(M + beta*h*h*K)*K;
A_pred = -1*inv(M)*K;

for i=1:n
  a_curr = A_pred*x_curr;
  x_pred_next = x_curr + h*v_curr + 0.5*h*h*(1-2*beta)*a_curr;
  v_pred_next = v_curr + h*(1-gamma)*a_curr;
  a_next = A*x_pred_next;
  x_next = x_pred_next + h*h*beta*a_next;
  v_next = v_pred_next + h*gamma*a_next;
  e(i) = 0.5*v_next.'*M*v_next + 0.5*x_next.'*K*x_next;
  x_curr = x_next;
  v_curr = v_next;
  norm_state(i) = norm(x_curr);
endfor
endfunction