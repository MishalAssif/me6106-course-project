\section{Basic Defintions}

We define a Discrete Lagrangian on a configuration space $\Q$ to be a $C^2$ smooth function $L_d : \Q \times \Q \mapsto \R$. In practice, $L_d$ is obtained as a discrete approximation of a continous Lagrangian $L$. We shall see this in an example. Since we have defined a discretization of the Lagrangian, it is quite natural to now look for a discretization of the action integral. We define the action sum $\actionMap_d : \Q^{N+1} \mapsto \R$ to be
\begin{equation*}
\actionMap_d(q) = \sum_{k = 0}^{N}L_d(q_k, q_{k+1})
\end{equation*}

\par We say that the discrete trajectory $\{q_k\}_{k=0}^{N}$ satisfies the \textit{Discrete Euler Lagrange (DEL) equations} if
\begin{equation}\label{DEL}
D_1L_d(q_{k+1}, q_{k+2}) + D_2L_d(q_k, q_{k+1}) = 0
\end{equation} 
for all $k = 1,...,N-1$, where $D_iL_d$ is the partial derivative with respect to the $i^{th}$ argument. We define the discrete evolution operator $\phi : \Q \times \Q \mapsto \Q \times \Q$ as
\begin{equation}\label{disc_evolution}
D_1L_d(\phi(q_{k}, q_{k+1})) + D_2L_d(q_k, q_{k+1}) = 0.
\end{equation}
It is not obvious that given any $L_d$ one can find a discrete evolution operator, because it is not necessary that $D_1L_d$ is invertible. Under some regularity assumptions on $L_d$ one can indeed show that if $q_{k+1}$ is close enough to $q_k$ then $\phi(q_k, q_{k+1})$ is well defined. For small enough time steps, $q_{k+1}$ will be close enough to $q_k$ and thus the discrete evolution operator will be well defined.
\par We define the discrete Fiber derivative (or discrete Legendre transform) $\F L_d : \Q \times \Q \mapsto T^{*}\Q$ given by
\begin{equation*}
\F L_d(q_0, q_1) = (q_1, D_2L_d(q_0, q_1))
\end{equation*}

\begin{example}\label{disc_lag}
\normalfont Recall the Lagrangian L defined in Example \ref{Mass-Spring}. Consider the Discrete lagrangian $L_d^{\alpha} : \Q \times \Q \mapsto \R$ given by
\begin{equation}
L_d^{\alpha}(q_0, q_1) = hL\bigl( (1-\alpha)q_0 + \alpha q_1, \frac{q_1 - q_0}{h} \bigr)
\end{equation}
where $h \in \R_{+}$ is the time step and $\alpha$ is an interpolation parameter. Using the given form of $L$, we get
\begin{align*}
&L_d^{\alpha}(q_0, q_1) = h\frac{1}{2}\bigl( \frac{q_1 - q_0}{h} \bigr)^T M \bigl( \frac{q_1 - q_0}{h} \bigr) - h\frac{1}{2}((1 - \alpha)q_0 + \alpha q_1)^TK((1 - \alpha)q_0 + \alpha q_1),\\
&D_2L_d^{\alpha}(q_0, q_1) = \frac{1}{h}M(q_1 - q_0) - h \alpha K((1 - \alpha)q_0 + \alpha q_1),\\
&D_1L_d^{\alpha}(q_1, q_2) = \frac{1}{h}M(q_1 - q_2) - h (1 - \alpha) K((1 - \alpha)q_1 + \alpha q_2).
\end{align*}
\begin{align*}
&D_2L_d^{\alpha}(q_0, q_1) + D_1L_d^{\alpha}(q_1, q_2) = 0 \\
&\iff -(M + h^2\alpha(1-\alpha)K)q_2 + M(2q_1 - q_0) - h^2K\bigl(\alpha(1-\alpha)q_0 + (\alpha^2 + (1-\alpha)^2)q_1 \bigr) = 0 \\
&\iff (M + h^2\alpha(1-\alpha)K)q_2 =  M(2q_1 - q_0) - h^2K\bigl(\alpha(1-\alpha)q_0 + (\alpha^2 + (1-\alpha)^2)q_1 \bigr) \\
&\iff q_2 = (M + h^2\alpha(1-\alpha)K)^{-1}\bigl( M(2q_1 - q_0) - h^2K\bigl(\alpha(1-\alpha)q_0 + (\alpha^2 + (1-\alpha)^2)q_1 \bigr) \bigr).
\end{align*}
\begin{equation*}
\therefore \phi(q_0, q_1) = (q_1, (M + h^2\alpha(1-\alpha)K)^{-1}\bigl( M(2q_1 - q_0) - h^2K\bigl(\alpha(1-\alpha)q_0 + (\alpha^2 + (1-\alpha)^2)q_1 \bigr) \bigr))
\end{equation*}
The discrete Fiber derivative is given by 
\begin{align*}
\F L_d^{\alpha}(q_0, q_1) = (q_1, M\bigl(\frac{q_1-q_0}{h}\bigr) - h\alpha K((1-\alpha)q_0 + \alpha q_1))
\end{align*}
\end{example} 

\section{Invariants of the discrete flow}

\subsection{Discrete Lagrangian Energy}

The energy associated with a discrete Lagrangian is given by
\begin{equation}\label{disc_energy}
E_d(q_0, q_1, h) = -\frac{\partial}{\partial h}L_d(q_0, q_1, h)
\end{equation}
Even though we defined the discrete Lagrangian to be a function of just two arguments, it did contain the timestep as a third argument. The author apologizes for this abuse of notation.

\begin{example}
\normalfont For the discrete Lagrangian given in Example \ref{disc_lag}, the energy is given by
\begin{equation*}
E_d^\alpha(q_0, q_1) = \frac{1}{2}\bigl( \frac{q_1 - q_0}{h} \bigr)^TM\bigl( \frac{q_1 - q_0}{h} \bigr) + \frac{1}{2}((1-\alpha)q_0 + \alpha q_1)^TK((1-\alpha)q_0 + \alpha q_1).
\end{equation*}
In fact, in this example, we can write
\begin{equation*}
E_d^\alpha(q_0, q_1) = E((1-\alpha)q_0 + \alpha q_1, \frac{q_1 - q_0}{h}).
\end{equation*}
where $E$ is the energy associated with the Lagrangian $L$. 
\end{example}

\subsection{Discrete Lagrangian Symplectic form}

The Discrete Lagrangian Symplectic form $\Omega_{L_d} : \Q \times \Q \mapsto T^{*}(\Q \times \Q)$ is given by
\begin{equation}\label{disc_lag_symp_form}
\Omega_{L_d}(q_0, q_1) = \frac{\partial^2 L_d}{\partial q_0^i \partial q_0^j}(q_0, q_1) \exd q_0^i \wedge \exd q_1^j
\end{equation}

\begin{theorem}\label{disc_lag_symp_flow}
The Discrete Lagrangian evolution operator $\phi$ preserves the Discrete Lagrangian symplectic form, i.e.
\begin{equation}
\phi^{*}\Omega_{L_d} = \Omega_{L_d}
\end{equation}
\end{theorem}
For a proof of theorem \ref{disc_lag_symp_flow} we refer the reader to \cite[Section~1.3.2]{MaWe2001}.

\begin{example}
\normalfont For the discrete Lagrangian given in Example \ref{disc_lag}, the discrete Lagrangian symplectic form is given by
\begin{equation}
\Omega_{L_d^\alpha}(q_0, q_1) = -(\frac{1}{h}M_{ij} + h\alpha(1-\alpha)K_{ij})\exd q_0^i \wedge \exd q_1^j
\end{equation}
\end{example}

\begin{remark}\label{disc_symp_nature}
\normalfont Just like in the continous case, we can show here also that
\begin{equation}
\Omega_{L_d} = (\F L_d)^{*}\Omega,
\end{equation}
where $\Omega$ is the Liouville form on $T^{*}\Q$ (See \cite[Section~1.5.1]{MaWe2001} for a proof). So, in discrete time also we have a similar interpretation of the invariance of the symplectic form under the flow as in Remark \ref{symp_nature}. 
\end{remark}

\section{Variational Integrators}

\par Suppose we have a mechanical system evolving on $\Q$ with Lagrangian $L$. The reader might already have guessed how we go about constructing Variational integrators. What we would like is the discrete action sum to approximate the action integral, which can be restated as
\begin{equation}
L_d(q_0, q_1, h) \approx \int_{0}^{h}L(q_{EL}(t), \dot{q}_{EL}(t))
\end{equation}
where $q_{EL}(t)$ is the curve satisfying the EL equations and $q_{EL}(0) = q_0, q_{EL}(h) = q_1$. The solution to the DEL equations corresponding to $L_d$ is what we expect will give us an approximation of the continous EL trajectory.

In the preceding sections, the reader might have noticed that our presentation has almost mirroed that in the continous time case. The only thing which did not have a discrete time correspondence is the Hamilton's principle. We shall fill this void here. We first define the Exact Discrete Lagrangian $L_d^E : T\Q \mapsto \R$ as
\begin{equation}
L_d^E(q_0, q_1, h) = \int_{0}^{h}L(q_{EL}(t), \dot{q}_{EL}(t))
\end{equation}
where $q_{EL}(t)$ is the curve satisfying the EL equations and $q_{EL}(0) = q_0, q_{EL}(h) = q_1$. The solution to the DEL equations corresponding to $L_d$ is what we expect will give us an approximation of the continous EL trajectory.

\begin{theorem}\label{correspondence}
A discrete trajectory $\{q_k\}_{k=0}^{N}$ satisfies the DEL equations corresponding to $L_d^E$ iff there exists a continous trajectory $q(t)$ satisfying the EL equations corresponding to $L$ such $q_k = q(kh)$.
\end{theorem}

For a proof of Theorem \ref{correspondence}, we refer the reader to \cite[Section~1.6]{MaWe2001}. What we would like to highlight here is that if know $L_d^E$, then by solving the DEL equations we can find an \textit{exact} discretization of the original continous time trajectory. It might seem like we have found the most ideal integrator ever; unfortunately this is not the case. It is virtually impossible to find $L_d^E$ for any non-trivial problem. Nevertheless, the exact discrete lagrangian is an important object in the error analysis of variational integrators. ). Many important properties of the integrator, like convergence and order, can be expresses in terms of the error between the chosen discrete Lagrangian $L_d$ and the exact discrete Lagrangian $L_d^E$ (See \cite[Chapter~2]{MaWe2001}.

\par We have already seen a class of Variational integrators for the Mass-Spring system in Example \ref{disc_lag}. We will end this chapter with the construction of another class of variational integrators. It is to be noted that the expression for the discrete evolution operator in Example \ref{disc_lag} is rather long and unwieldy. To avoid this, we introduce the evaluated accelaration notation :
\begin{equation}
a_{k+\alpha} = -M^{-1}( K((1-\alpha)q_k + \alpha q_{k+1}) )
\end{equation}
With this, we can rewrite the expression in Example \ref{disc_lag} as
\begin{equation}
\frac{1}{h^2}(q_{k+2} - 2q_{k+1} + q_k) = (1-\alpha)a_{k+1+\alpha} + \alpha a_{k+\alpha}
\end{equation}
\par Now we define the new family of integrators
\begin{align}
L_d^{sym, \alpha}(q_0, q_1) &= \frac{h}{2}L((1-\alpha)q_0 + \alpha q_1, \frac{q_1 - q_0}{h}) + \frac{h}{2}L(\alpha q_0 + (1-\alpha) q_1, \frac{q_1 - q_0}{h}) 
\end{align}
It can be easily seen that the DEL equations corresponding to $L_d^{sym, \alpha}$ is
\begin{equation}\label{sym-variational}
\frac{1}{h^2}(q_{k+2} - 2q_{k+1} + q_k) = \frac{1}{2}((1-\alpha)a_{k+1+\alpha} + \alpha a_{k+2-\alpha} + \alpha a_{k+\alpha} + (1-\alpha)a_{k+1-\alpha})
\end{equation}