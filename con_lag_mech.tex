\section{Basic Defintions}

We consider a mechanical system evolving on the configuration space $\Q = \R^n$ with associated state space given by $T\Q = T\R^n = \R^n \times \R^n$. The state space is just the collection of ordered pairs of generalized positions and velocities, the first element of the pair being position and the latter velocity. Similarly, the phase space of the system is given by $\coTBundle{\Q} = \coTBundle{\R^n} = \R^n \times \dual{(\R^n)}$, where $\dual{(\R^n)}$ is the dual space of $\R^n$, the set of covectors of $\R^n$. The phase space can be thought of as the set of ordered pairs of generalized positions and momenta. Momentum is treated as a covector because momentum acts on velocity linearly to give a real number, the kinetic energy scaled by a factor of two. Let $K : T\Q \mapsto \R$ and $V : \Q \mapsto \R$ denote the Kinetic and Potential energy of the sytem respectively. We define the Lagrangian $L: T\Q \mapsto \R$ to be
\begin{equation*}
L(q, \dot{q}) = K(q, \dot{q}) - V(q)
\end{equation*}
We assume the Lagrangian is $C^2$ smooth throughout the article. Given an interval $[0, T]$, we define the path space to be 
\begin{equation*}
\pathSpace{\Q} = \{q:[0, T] \mapsto Q | \ q \ \text{is a $C^2$ curve} \}
\end{equation*}
and the action map $\actionMap : \pathSpace{Q} \mapsto \R$ to be
\begin{equation*}
\actionMap(q) = \int_{0}^{T} L(q(t), \dot{q}(t)) dt.
\end{equation*}


\begin{theorem}[Hamilton's principle of least action] 
A mechanical system moves from configuration $q_0$ to $q_1$ between time $0$ to $T$ along the curve $q \in \pathSpace{\Q}$ which satisfies
\begin{equation}\label{eq: action_var}
\delta S(q)[\delta q] = 0
\end{equation}
for all variations $\delta q$ of $q$ with endpoints fixed at $q_0$ and $q_1$.
We shall call this curve the Euler-Lagrange curve between points $q_0$ and $q_1$. It is well know that the condition \ref{eq: action_var} is equivalent to 
\begin{equation}\label{eq: c_euler_lagrange}
\frac{d}{dt}\frac{\partial L}{\partial \dot{q}} - \frac{\partial L}{\partial q} = 0
\end{equation}
along the curve $q$.
\end{theorem}
In the above theorem, it is assumed that the reader is familiar with the variation of a functional. We refer the reader to \cite{Holm2009} for more details and a proof of the above theorem. 
\par Equation \ref{eq: c_euler_lagrange} is called the Continous time Euler Lagrange (EL) equation. Under some regularity assumptions on $L$, it is a second order ordinary differential equation which gives the equations of motion of a mechanical system. 
\par We shall denote the flow generted by the Euler Lagrange equations by $F_L^T : T\Q \mapsto \Q$. $F_L^T(q_0, \dot{q}_0)$ just gives the point on the configuration space that a system which at time $t = 0$ is at position $q_0$ with velocity $\dot{q}_0$ reaches at time $t = T$.
\par We define the Fiber derivative $\F L : T\Q \mapsto \coTBundle{\Q}$ to be
\begin{equation}\label{def: fiber_derivative}
\F L(q, \dot{q}) = (q, \frac{\partial L}{\partial \dot{q}}(q, \dot{q}))
\end{equation} 

\begin{remark}\label{fib_der}
\normalfont It is to be noted that the Fiber derivative is just the Legendre transform of $L$. There is another viewpoint to Classical mechanics; Hamiltonian mechanics. Hamiltonian mechanics describes the motion of a mechanical system on the phase space, as opposed to the state space in Lagrangian mechanics. The Fiber derivative acts as a bridge between Lagrangian and Hamiltonian mechanics. For most concepts defined in Lagrangian mechanics, there exists a counterpart in Hamiltonian mechanics, and more often than not, the Fiber derivative is used in some form to connect the two. Throughout this article, we will only focus on the Lagrangian viewpoint. 
\end{remark}
\begin{example}[Mass-Spring system]\label{Mass-Spring}
\normalfont Let the Lagrangian be
\begin{equation*}
L(q, \dot{q}) = \frac{1}{2}\dot{q}^TM\dot{q} - \frac{1}{2}q^TKq
\end{equation*}
\normalfont where $M = M^T \succ 0, K = K^T \succeq 0$. Then, the Euler Lagrange equations are
\begin{equation*}
M\ddot{q} +  Kq = 0.
\end{equation*}
This is the well known equations of motion of a Mass-Spring system. The Fiber derivative is given by
\begin{equation*}
\F L(q, \dot{q}) = (q, M\dot{q})
\end{equation*}
We see that in this case, the Fiber derivative is just the linear momentum of the system.
\end{example}

\section{Invariants of the Flow}

\subsection{Lagrangian Energy}

We define the energy associated with the Lagrangian $L$ to be $E : T\Q \mapsto \R$ given by
\begin{equation}\label{cont_enegy}
E(q, \dot{q}) = \frac{\partial L}{\partial \dot{q}}\dot{q} - L(q, \dot{q})
\end{equation}


\begin{theorem}\label{inv_energy}
The energy $E$ is invariant under the flow of $L$
\end{theorem}

\begin{proof}
\begin{align*}
\frac{dE}{dt}(q(t), \dot{q}(t)) = \ & \frac{\partial^2 L}{\partial \dot{q}^2}(\ddot{q}(t), \dot{q}(t)) + \frac{\partial^2 L}{\partial q \partial \dot{q}}(\dot{q}(t), \dot{q}(t)) + \frac{\partial L}{\partial \dot{q}}\ddot{q}(t) \\
&- \frac{\partial{L}}{\partial q}\dot{q}(t) - \frac{\partial{L}}{\partial \dot{q}}\ddot{q}(t)  \\
= \ & \frac{\partial^2 L}{\partial \dot{q}^2}(\ddot{q}(t), \dot{q}(t)) + \frac{\partial^2 L}{\partial q \partial \dot{q}}(\dot{q}(t), \dot{q}(t)) - \frac{\partial{L}}{\partial q}\dot{q}(t). \\
\frac{d}{dt}\frac{\partial L}{\partial \dot{q}} - \frac{\partial L}{\partial q} = \ & \frac{\partial^2 L}{\partial \dot{q} \partial \dot{q}}\ddot{q} +  \frac{\partial^2 L}{\partial q \partial \dot{q}}\dot{q} - \frac{\partial{L}}{\partial q} = 0.\\
\implies \frac{dE}{dt}(q(t), \dot{q}(t))  = \ & 0.
\end{align*}
\end{proof}

\begin{example}[Mass-Spring system continued]
\normalfont For the Mass-Spring system, energy $E$ is given by
\begin{align*}
E(q, \dot{q}) &= (2M\dot{q})^T\dot{q} - \dot{q}^TM\dot{q} + q^TKq \\
&= \dot{q}^TM\dot{q} + q^TKq 
\end{align*}
In this case, the Energy function is indeed the Total Energy of the Mass-Spring system.
\begin{align*}
\frac{dE}{dt}(q(t), \dot{q}(t)) &= 2\dot{q}^T(t)M\ddot{q}(t) + 2q^T(t)K\dot{q}(t) \\
&= 2q^T(t)(M\ddot{q}(t) + K\dot{q}(t)) \\
&= 0
\end{align*}
\end{example}

\subsection{Lagrangian Symplectic form}

The Lagrangian Symplectic form $\Omega_L : T\Q \mapsto T^*(T\Q)$ is given by
\begin{equation}\label{cont_lag_symp_form}
\Omega_L(q, \dot{q}) = \frac{\partial^2 L}{\partial q^i \partial \dot{q}^j}\exd q^i \wedge \exd q^j + \frac{\partial^2 L}{\partial \dot{q}^i \partial \dot{q}^j}\exd \dot{q}^i \wedge \exd q^j  
\end{equation}

\begin{theorem}\label{lag_symp_flow}
The Lagrangian flow preserves the Lagrangian Symplectic form, i.e.
\begin{align*}
(F_L^T)^*(\Omega_L) = \Omega_L, \ \forall T \in \R.
\end{align*}
\end{theorem}

For a proof of theorem \ref{lag_symp_flow}, we refer the reader to \cite[Section~1.2.3]{MaWe2001}.

\begin{example}[Mass-Spring system continued]
\normalfont Consider the Lagrangian
\begin{equation*}
L(q, \dot{q}) = \frac{1}{2}\dot{q}^TM\dot{q} - \frac{1}{2}q^TKq
\end{equation*}
Then, the Lagrangian Symplectic form is given by
\begin{equation*}
\Omega_L(q, \dot{q}) = M_{ij} \exd \dot{q}^i \wedge \exd q^j
\end{equation*}
\end{example}

\begin{remark}\label{symp_nature}
\normalfont The definition of the Lagrangian symplectic form seems rather cryptic and non-intuitive. It is not at all clear what the physical significance of the Lagrangian symplectic form is. However, as we mentioned in Remark \ref{fib_der}, there exists a counterpart to the Lagrangian symplectic form on the phase space, which has a very nice geometric interpretation. It can be shown that
\begin{equation}\label{symp_form_fib_der}
\Omega_L = (\F L)^* \Omega,
\end{equation} 
where $\Omega$ is the Liouville form on $T^{*}\Q$ given by
\begin{equation*}
\Omega(q, p) = \exd q^i \wedge \exd p^i.
\end{equation*}
So, the fact that the Lagrangian flow preserves the Lagrangian symplectic form implies that it also preserves Liouville form on the phase space. This means that the Lagrangian flow preserves the area of any 2 dimensional smooth surface in the phase space. For a more detailed discussion on this, and a detailed proof of what we have mentioned above, please see \cite[Section~VI.2]{Hairer2006}.
\end{remark}

With this remark, we end our discussion on continous time Lagrangian mechanics. There is another important invariant of the Lagrangian flow, the momentum map. In the context of Structural dynamics, except in the case of very simple examples, we usually won't be able to find a momentum map. Because of this, and for the sake of brevity, we shall not be looking at momentum maps and their invariance in this article. We refer the interested reader to \cite[Section~1.2.4]{MaWe2001}.